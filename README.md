# Single-photon Orbital Angular Momentum for Downlink Satelite Communications 

- [Introduction](#introduction)
  - [Structure](#structure)

## Introduction
This repository contains the MATLAB code used for simulating the propagation of Laguerre-Gaussian modes carrying Orbital Angular Momentum (OAM) through atmospheric turbulence in an optical, downlink (space to ground) satellite communications scenario. 

The propagation is performed numerically using the **Multiple Phase Screen (MPS)** method as implemented in _Numerical Simulation of Optical Wave Propagation with examples in MATLAB_ by J. D. Schmidt.

The main script used for the simulations is _mps_oam.m_.

### Structure
```bash
.
├─ .gitattributes
├─ .gitignore
├─ Code                             # Folder containing all code files
│  ├─ afgl_wk.m
│  ├─ ang_spec_multi_prop.m
│  ├─ ang_spec_multi_prop_vac.m
│  ├─ atmo.m
│  ├─ atmo_compo.m
│  ├─ atmo_p.m
│  ├─ atmo_temp.m
│  ├─ circ.m
│  ├─ corr2_ft.m
│  ├─ ft2.m
│  ├─ ft_phase_screen.m
│  ├─ ft_sh_phase_screen.m
│  ├─ f_n.m
│  ├─ HV_model.m
│  ├─ ift2.m
│  ├─ int_tau.m
│  ├─ LICENSE
│  ├─ mps_oam.m
│  ├─ mps_oam_v2.m
│  ├─ mps_oam_vacuum.m
│  ├─ oam.m
│  ├─ oam_statistical.ipynb
│  ├─ oam_statistical.m
│  ├─ pt_turb_prop_smith.m
│  ├─ Quantum_GPU_Enabled.m
│  ├─ std_atmo.m
│  ├─ tester_standard_atmosphere.m
│  └─ theoretical_plots.m
├─ Documentation                    # Thesis rendered text
│  └─ thesis.pdf
├─ Images                           # Images used for the README
│  ├─ lg1.png
│  ├─ lg4.png
│  ├─ oam.PNG
│  ├─ per_lg4.png
│  ├─ python_dr0025.png
│  └─ QuantumFirstRun.png
├─ LICENSE
├─ README.md
└─ Results                          # Simulation results figures
   ├─ QuantumFirstRun.png
   ├─ QuantumSecondRun.png
   ├─ Quantum_00105_100.fig
   ├─ Quantum_00105_500.fig
   └─ Theoretical plots
      └─ super_gaussian.eps

```
%% Program initialization
% Add all sub-folders to path 
addpath standard_atmosphere;
addpath structure_parameter;
addpath beam_generation;
addpath numerical_propagation;
addpath detection_probability;
addpath 'detection_probability/convolution_fft';

% Clear workspace
clear;
% 
% % Close previous figures
% close all;

%----Input----%

A = importfile('TestReport.txt', 2, inf);
eleva = table2array(A(:,1));
La = table2array(A(:,2));
timea = table2array(A(:,3));
Ha = table2array(A(:,4));
h0a = table2array(A(:,5));
utc_timea = table2array(A(:,6));

%----Parameters----%

%% Channel parameters

Cn0 = 9.6*10^(-14);
RE = 6371*10^3;

L_max = max(La);

%% Optical parameters
lambda = 1550*10^(-9); % Optical wavelength
k = 2*pi/lambda; % Optical wavenumber
w0 = sqrt(L_max*lambda/pi); % Beamwaist radius for minimum diffraction
w0 = 15*10^(-2); % Beamwaist radius
zR = pi*(w0^2)/lambda; % Rayleigh range
p = 0; % Radial mode number
la = [0 1 2 3 4]; % OAM quantum number
% la = 0;

Pla = zeros(length(la), length(La));
Pla_max = zeros(length(la), length(La));
runs = length(La);

for i = 1:length(la)
for j = 1:runs

l = la(i);
thetaz = 90 - eleva(j);
L = La(j);
H = Ha(j);
h0 = h0a(j);

%% Geometry parameters 
r0lrms_tra = sqrt((l+1)/2)*w0;
r0l_tra = sqrt(2)*r0lrms_tra;
wL = w0*sqrt(1+(L/zR)^2);
r0lrms_rec = sqrt((l+1)/2)*wL;
r0l_rec = sqrt(2)*r0lrms_rec;
D1 = 2*r0l_tra; % Diameter of the source aperture [m]
D2 = 2*r0l_rec; % Diameter of the observation aperture [m]
D2 = 2.3;
r0l_0 = sqrt((l+1)/2)*w0;
r0l_L = sqrt((l+1)/2)*wL;

%% NASA US Standard atmosphere calculations

h_division = 100; % Must be a multiple of ten
h = h0:h_division:H;
[temp, press, lapse_rate] = std_atmo(h0, H, h_division);

%% AFGL+WK model

S = zeros(1, length(h)); % Wind shear
Cn2 = afgl_wk(h0, H, h_division, temp, press, lapse_rate, Cn0, S);

% Calculate Cn2 from Hufnagel-Valley model (for comparison)
urms = 6;
Cn2_hv = HV_model(h0, H, h_division, urms, Cn0);

%% Sampling constraints

rF = (0.423*(k^2)*h_division*sum(Cn2(1:end).*((h(1:end) + RE)./sqrt((h(1:end) + RE).^2 - (RE + h0)^2*sind(thetaz)^2))))^(-3/5); % Fried's parameter for plane wave
rytov = 0.563*(k^(7/6))*h_division*sum(Cn2(1:end).*((h(1:end) + RE)./sqrt((h(1:end) + RE).^2 - (RE + h0)^2*sind(thetaz)^2)).*((h(1:end) - h(1)).^(5/6)));

c = 1;
R = L*(1+(zR/L)^2);
sampl_scaling = 8;

[delta1, deltan, Nmin] = ...
    sampl_constraints(c, lambda, L, sampl_scaling, D1, D2, rF, R, 'no');

N = Nmin % Number of grid points
zmax = min([delta1 deltan])^2 * N /lambda;
nmin = ceil(L / zmax) + 1;
if (3*nmin<10)
    n = 10; % Number of planes
else
    n = 3*nmin;
end
% n = 3*nmin;

% Coherence radii of propagation planes
r0scrn_all(:, j*i) = phase_screen_r0(L, rF, rytov, n, lambda);

end
end

save('r0scrn_all_gmat_a3sat_c1.mat', 'r0scrn_all')
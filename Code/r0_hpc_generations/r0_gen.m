%% Program initialization
% Add all sub-folders to path 
addpath standard_atmosphere;
addpath structure_parameter;
addpath beam_generation;
addpath numerical_propagation;
addpath detection_probability;
addpath 'detection_probability/convolution_fft';

% Clear workspace
clear;

% Close previous figures
% close all;

% Set linewidth for figures
set(0, 'DefaultLineLineWidth', 1.5);

%----Parameters----%

%% Channel parameters
h0a = [1000 3000]; % Ground station altitude
h0_min = min(h0a,[],'all');
h0 = 3000;
Ha = (150:50:500)*10^3; % Satellite altitude
H_max = max(Ha,[],'all');
% Ha = 500*10^3;
Cn0 = 9.6*10^(-14);
thetaza = (0:10:70);
thetaz_max = max(thetaza,[],'all');
thetaza = 0;

L_max = (H_max-h0_min)/cosd(thetaz_max);

%% Optical parameters
lambda = 1550*10^(-9); % Optical wavelength
k = 2*pi/lambda; % Optical wavenumber
w0 = sqrt(L_max*lambda/pi); % Beamwaist radius for minimum diffraction
w0 = 15*10^(-2); % Beamwaist radius
zR = pi*(w0^2)/lambda; % Rayleigh range
p = 0; % Radial mode number
la = [0 1 2 3 4]; % OAM quantum number
% la = 3;

if (length(thetaza) > 1)
    Pla = zeros(length(la), length(thetaza));
    Pla_max = zeros(length(la), length(thetaza));
    runs = length(thetaza);
else
    Pla = zeros(length(la), length(Ha));
    Pla_max = zeros(length(la), length(Ha));
    runs = length(Ha);
end

tic
for i = 1:length(la)
for j = 1:runs

l = la(i);
if (length(thetaza) > 1)
    thetaz = thetaza(j);
    H = Ha;
elseif (length(Ha) > 1)
    thetaz = thetaza;
    H = Ha(j);
end
L = (H-h0)/cosd(thetaz);

%% Geometry parameters 
r0lrms_tra = sqrt((l+1)/2)*w0;
r0l_tra = sqrt(2)*r0lrms_tra;
wL = w0*sqrt(1+(L/zR)^2);
r0lrms_rec = sqrt((l+1)/2)*wL;
r0l_rec = sqrt(2)*r0lrms_rec;
D1 = 2*r0l_tra; % Diameter of the source aperture [m]
D2 = 2*r0l_rec; % Diameter of the observation aperture [m]
% D2 = 2.3;
r0l_0 = sqrt((l+1)/2)*w0;
r0l_L = sqrt((l+1)/2)*wL;

%% NASA US Standard atmosphere calculations

h_division = 10; % Must be a multiple of ten
h = h0:h_division:H;
[temp, press, lapse_rate] = std_atmo(h0, H, h_division);

%% AFGL+WK model

S = zeros(1, length(h)); % Wind shear
Cn2 = afgl_wk(h0, H, h_division, temp, press, lapse_rate, Cn0, S);

% Calculate Cn2 from Hufnagel-Valley model (for comparison)
urms = 6;
Cn2 = HV_model(h0, H, h_division, urms, Cn0);

%% Sampling constraints

rF = (0.423*(k^2)*secd(thetaz)*h_division*sum(Cn2(1:end)))^(-3/5); % Fried's parameter for plane wave
rytov = 0.563*(k^(7/6))*(secd(thetaz)^(11/6))*h_division*sum(Cn2(1:end).*((h(1:end) - h(1)).^(5/6))); % Rytov variance for plane wave

c = 2;
R = L*(1+(zR/L)^2);
% R = L;
sampl_scaling = 8;

[delta1, deltan, Nmin] = ...
    sampl_constraints(c, lambda, L, sampl_scaling, D1, D2, rF, R, 'no');

toc
N = Nmin % Number of grid points
tic
zmax = min([delta1 deltan])^2 * N /lambda;
nmin = ceil(L / zmax) + 1;
if (3*nmin<10)
    n = 10; % Number of planes
else
    n = 3*nmin;
end
% n = 3*nmin;

% Coherence radii of propagation planes
r0scrn_all(:, j*i) = phase_screen_r0(L, rF, rytov, n, lambda);
end
end
save('r0scrn_all_h3000_w015_HV.mat', 'r0scrn_all')
close all;
clear all;

%% Optical parameters
lambda = 1550*10^(-9); % Optical wavelength [m]
k = 2*pi / lambda; % Optical wavenumber [rad/m]
w0 = 15*10^(-2); % Beamwaist radius
zR = pi*(w0^2)/lambda; % Rayleigh range
p = 0; % Radial mode number
Lp = 1; % Generalized Laguerre polynomial of order zero
la = [0 1 2 3 4]; % OAM quantum number
l = la(1);

%% Channel parameters
h0a = [0 1000 3000]; % Ground station altitude
h0 = 1000;
Ha = (150:50:500)*10^3; % Satellite altitude
H = 150*10^3; 
thetaz = 0; % Zenith angle
Cn0 = 9.6*10^(-14); % Reference structure parameter
L = (H-h0)/cos(thetaz); % Propagation distance

%% Transmitter/receiver apertures
r0lrms_tra = sqrt((max(la)+1)/2)*w0;
r0l_tra = sqrt(2)*r0lrms_tra;
wDz = w0*sqrt(1+(L/zR)^2);
r0lrms_rec = sqrt((max(la)+1)/2)*wDz;
r0l_rec = sqrt(2)*r0lrms_rec;
D1 = r0l_tra; % Diameter of the source aperture [m]
D2 = r0l_rec; % Diameter of the observation aperture [m]

%% NASA US Standard atmosphere calculations
division = 250;
h = h0:division:H;
index_o1km = find(h == 1000);
h_o1km = h(index_o1km + 1 : end);
h_u1km = h(1:index_o1km);

% Calculate the temperature vector
temp = zeros(1, length(h_o1km));
for i=1:length(h_o1km)
    temp(i) = atmo_temp(h_o1km(i)*10^(-3));
end

% Calculate the pressure vector
index_o86km = find(h_o1km == 86000);
temp_o86km = temp(index_o86km : end);
h_o86km = h_o1km(index_o86km : end);
h_u86km = h_o1km(1 : index_o86km - 1);
sum_n = sum(atmo_compo(H*10^(-3), division*10^(-3)), 2);
press_o86km = atmo_p(h_o86km*10^(-3), temp_o86km, sum_n');
press_u86km = atmo_p(h_u86km*10^(-3));
press = 0.01*[press_u86km press_o86km];

% Calculate the lapse rate
lapse_rate = zeros(1,length(h_o1km));
for i=1:length(h_o1km)-1
    lapse_rate(i) = (10^3)*(temp(i+1) - temp(i))/(h_o1km(i+1) - h_o1km(i));
end
lapse_rate(end) = lapse_rate(end - 1);

% Plot results
figure;
plot(h_o1km*10^(-3), press);
title('NASA US Standard atmosphere model')
xlabel('Altitude (km)')
ylabel('Pressure (mba)')
figure;
plot(h_o1km*10^(-3), temp);
title('NASA US Standard atmosphere model')
xlabel('Altitude (km)')
ylabel('Temperature (K)')
figure;
plot(h_o1km*10^(-3), lapse_rate)

%% AFGL+WK model
gamma = 9.8; % [K/km]
g = 9.8;
S = zeros(1, length(h_o1km));
Y = zeros(1, length(h_o1km));
for i = 1 : length(h_o1km)
    if (h_o1km(i) < 5472)
        Y(i) = 2.9767 + 27.9804 * S(i) + 2.9012 * lapse_rate(i) + ...
            1.1843 * lapse_rate(i)^2 + 0.1741 * lapse_rate(i)^3 + ...
            0.0086 * lapse_rate(i)^4;
    elseif (h_o1km(i) >= 5472 && h_o1km(i) < 14172)
        Y(i) = 0.7152 + 30.6024 * S(i) + 0.0003 * lapse_rate(i) - ...
            0.0057 * lapse_rate(i)^2 - 0.0016 * lapse_rate(i)^3 + ...
            0.0001 * lapse_rate(i)^4;
    else 
        Y(i) = 0.6763 + 8.1569 * S(i) - 0.0536 * lapse_rate(i) + ...
            0.0084 * lapse_rate(i)^2 - 0.0007 * lapse_rate(i)^3 + ...
            0.00002 * lapse_rate(i)^4;
    end
end

href = 10; 
Cn2_u1km = Cn0 * (h_u1km/href).^(-2/3); % nighttime
N2 = ((10^(-3)) * g * (lapse_rate + gamma)) ./ temp;
M = (-79 * 10^(-6) * press .* N2) ./ (g * temp);
Cn2_o1km = 2.8 * (M.^2) * (0.1^(4/3)) .* 10.^(Y);
Cn2 = [Cn2_u1km Cn2_o1km];

% Plot the structure parameter
figure; 
semilogx(Cn2, h*10^(-3));
xlabel('Structure parameter (m^{-2/3})')
ylabel('Altitude (km)')
xlim([10^(-22) 10^(-13)])
ylim([0 35])

%% Vacuum propagation
% Sampling constraints
rF = inf; % Vacuum case
c = 2;
R = L*(1+(zR/L)^2);

D1p = 2*r0l_tra + c*lambda*L/rF;
D2p = 2*r0l_rec + c*lambda*L/rF;

delta1_max = lambda*L/D2p;
n = 1;
while round(delta1_max, n) == 0
    n = n + 1;
end
if round(delta1_max, n) > delta1_max
    delta1 = 2*delta1_max - round(delta1_max, n);
else
    delta1 = round(delta1_max, n);
end
delta1 = delta1/3;
deltan_max = lambda*L/D1p - D2p*delta1/D1p;
deltan = deltan_max/10; % observation-plane grid spacing [m]
Nmin = 2^(ceil(log2(D1p/2/delta1 + D2p/2/deltan + lambda*L/2/delta1/deltan)));
N = Nmin; % number of grid points
Dz_max = (N/lambda)*(min([delta1 deltan]))^2;
nmin = ceil(L/Dz_max) + 1;
n = nmin; % number of partial propagations

% Grid generation
[x1, y1] = meshgrid((-N/2 : N/2-1) * delta1);
[theta1, r1] = cart2pol(x1, y1);

z = 0;
w = w0*sqrt(1+(z(1)/zR)^2);
% Lp = -(2*(r1.^2)/w^2) + 1;
phi_in = exp(1j*l*theta1).*2*sqrt(factorial(p)/factorial(p+abs(l)))*(1/w).*((r1*sqrt(2)/w).^abs(l)).*exp(-r1.^2/w^2)*Lp.*exp(1j*k*r1.^2*z/(2*(z^2+zR^2)))*exp(-1j*(2*p+abs(l)+1)*atan(z/zR))/sqrt(2*pi);

z = (1 : n-1) * L / (n-1);

sg = exp(-(x1/(0.47*N*delta1)).^16).* exp(-(y1/(0.47*N*delta1)).^16);
t = repmat(sg, [1 1 n]);
[xn, yn, phi_vac] = ang_spec_multi_prop(phi_in, lambda, delta1, deltan, z, t);
[thetan, rn] = cart2pol(xn, yn);
% Collimate the beam
phi_vac = phi_vac .* exp(-1i*pi/(lambda*R)*(xn.^2+yn.^2));

w = wDz;
% Lp = -(2*(rn.^2)/w^2) + 1;
Uout_theor = exp(1j*l*thetan).*2*sqrt(factorial(p)/factorial(p+abs(l)))*(1/w).*((rn*sqrt(2)/w).^abs(l)).*exp(-rn.^2/w^2)*Lp.*exp(1j*k*rn.^2*L/(2*(L^2+zR^2)))*exp(-1j*(2*p+abs(l)+1)*atan(L/zR))/sqrt(2*pi);

% Plot received intensity
figure;
plot(xn(N/2+1,:), abs(phi_vac(N/2+1,:)),'r-o');
hold on; 
plot(xn(N/2+1,:), abs(Uout_theor(N/2+1,:)));
xlim([-D2/2 D2/2])
legend('Partial propagations', 'Theoretical')

figure;
[C,h_plot]=contourf(xn,yn,abs(phi_vac),100);
colorbar
colormap('hot')
h_plot.LineStyle='none';

% Theoretical modulus of the complex degree of coherence
w = sqrt(2*p + abs(l) + 1)*wDz;
F = L*(1+(zR/L)^2);
Theta = 1 + L/F;
Lambda = 2*L/k/w^2;
sR2 = 0;

% MCF2_gauss = (Theta^2 + Lambda^2)*exp(-2*(rn/w).^2);
% MCF2_gauss = ((w0/w)^2)*exp(-2*(rn/w).^2); % Needs scaling to be correct
% MCF2_gauss = Uout_theor.*conj(Uout_theor);
% MCF2_gauss = (2/pi/w^2)*exp(-2*(rn/w).^2);

T = 1.33*sR2*Lambda^(5/6);
q = 1.22*sR2^(6/5);
if (Theta >= 0)
    alpha = (1 - Theta^(8/3))/(1 - Theta);
else
    alpha = (1 + abs(Theta)^(8/3))/(1 - Theta);
end
MCF2_gauss = ((w0/w)^2)*exp(-T)*exp(-(Lambda*k*rn.^2)/4/L - 3*alpha*(((q*k*rn.^2)/L)^(5/6))/8);

mask = circ(xn/D2, yn/D2, 1);
mask = ones(N, N);
MCF2_vac = corr2_ft(phi_vac, phi_vac, mask, deltan);

figure; 
plot(xn(N/2+1, :), abs(MCF2_gauss(N/2+1, :))/max(abs(MCF2_gauss(N/2+1, :))), 'r--');
hold on;
plot(xn(N/2+1, :), abs(MCF2_vac(N/2+1, :))/max(abs(MCF2_vac(N/2+1, :))), 'go');
xlim([-D2/2 D2/2])
legend('Theoretical (vacuum formula)', 'Partial propagations')

figure;
[C,h_plot]=contourf(xn,yn,abs(MCF2_vac)/max(abs(MCF2_vac), [], 'all'),100);
colorbar
colormap('hot')
h_plot.LineStyle='none';

figure;
[C,h_plot]=contourf(xn,yn,abs(MCF2_gauss)/max(abs(MCF2_gauss), [], 'all'),100);
colorbar
colormap('hot')
h_plot.LineStyle='none';

%% Atmospheric turbulence propagation
% Sampling constraints
% rF = (0.423*(k^2)*secd(thetaz)*division*sum(Cn2(2:end).*((h(2:end)/L).^(5/3))))^(-3/5); % Fried's parameter (Coherence radius) for spherical wave
rF = (0.423*(k^2)*secd(thetaz)*division*sum(Cn2(2:end)))^(-3/5); % Fried's parameter for plane wave
% rytov = 0.563*(k^(7/6))*secd(thetaz)*division*sum(Cn2(2:end).*((h(2:end)/L).^(5/6)).*((1 - h(2:end)/L).^(5/6))); % Rytov variance for spherical wave
rytov = 0.563*(k^(7/6))*secd(thetaz)*division*sum(Cn2(2:end).*((1 - h(2:end)/L).^(5/6))); % Rytov variance for plane wave
c = 2;
R = L*(1+(zR/L)^2);

D1p = 2*r0l_tra + c*lambda*L/rF;
D2p = 2*r0l_rec + c*lambda*L/rF;

delta1_max = lambda*L/D2p;
n = 1;
while round(delta1_max, n) == 0
    n = n + 1;
end
if round(delta1_max, n) > delta1_max
    delta1 = 2*delta1_max - round(delta1_max, n);
else
    delta1 = round(delta1_max, n);
end
delta1 = delta1/2;
deltan_max = lambda*L/D1p - D2p*delta1/D1p;
deltan = deltan_max/7; % observation-plane grid spacing [m]
Nmin = 2^(ceil(log2(D1p/2/delta1 + D2p/2/deltan + lambda*L/2/delta1/deltan)));
N = Nmin % number of grid points
Dz_max = (N/lambda)*(min([delta1 deltan]))^2;
nmin = ceil(L/Dz_max) + 1;
n = nmin; % number of partial propagations

% Phase screen generation
L0 = inf;
l0 = 0;

[x1, y1] = meshgrid((-N/2 : N/2-1) * delta1);
[theta1, r1] = cart2pol(x1, y1);
z = 0;
w = w0*sqrt(1+(z/zR)^2);
phi_in = exp(1j*l*theta1).*2*sqrt(factorial(p)/factorial(p+abs(l)))*(1/w).*((r1*sqrt(2)/w).^abs(l)).*exp(-r1.^2/w^2)*Lp.*exp(1j*k*r1.^2*z/(2*(z^2+zR^2)))*exp(-1j*(2*p+abs(l)+1)*atan(z/zR))/sqrt(2*pi);

z = (1 : n-1) * L / (n-1);
zt = [0 z]; % propagation plane locations
Delta_z = zt(2:n) - zt(1:n-1); % propagation distances
% grid spacings
alpha = zt / zt(n);
delta = (1-alpha) * delta1 + alpha * deltan;

% generate a random draw of an atmospheric phase screen
[phz_lo, phz_hi] ...
= ft_sh_phase_screen(rF, N, delta(1), L0, l0);
phz = phz_lo + phz_hi;

% Print random atmospheric screen
figure;
[C,h_plot]=contourf(x1,y1,phz,100);
colorbar
colormap('gray')
h_plot.LineStyle='none';

nscr = n;
A = zeros(2, nscr); % matrix
alpha = (0:nscr-1) / (nscr-1);
A(1,:) = alpha.^(5/3);
A(2,:) = (1 - alpha).^(5/6) .* alpha.^(5/6);
b = [rF.^(-5/3); rytov/1.33*(k/L)^(5/6)];
% initial guess
x0 = (nscr/3*rF * ones(nscr, 1)).^(-5/3);
% objective function
fun = @(X) abs(sum((A*X(:) - b).^2));
% constraints
xl = zeros(nscr, 1);
rmax = 0.1; % maximum Rytov number per partial prop
xu = rmax/1.33*(k/L)^(5/6) ./ A(2,:);
xu(A(2,:)==0) = 50^(-5/3)
[X,fval,exitflag,output] = fmincon(fun,x0,[],[],[],[],xl,xu)
% check screen r0s
r0scrn = X.^(-3/5)
r0scrn(isinf(r0scrn)) = 1e6;
% check resulting r0sw & rytov
bp = A*X(:); [bp(1)^(-3/5) bp(2)*1.33*(L/k)^(5/6)]
[rF rytov]

% Initialize arrays for propagated fields,
% aperture mask, and MCF
phi_out_sum = zeros(N, N);
MCF2 = zeros(N);
sg = exp(-(x1/(0.47*N*delta1)).^16).* exp(-(y1/(0.47*N*delta1)).^16);
sg = repmat(sg, [1 1 n]);

% Initialize array for phase screens
phz = zeros(N, N, n);
nreals = 10; % number of random realizations
% Pl = zeros(1, nreals);

for idxreal = 1 : nreals % loop over realizations
    idxreal
    % loop over screens
    for idxscr = 1 : 1 : n
        [phz_lo, phz_hi] = ft_sh_phase_screen(r0scrn(idxscr), N, delta(idxscr), L0, l0);
        phz(:,:,idxscr) = phz_lo + phz_hi;
    end
    % simulate turbulent propagation
    [xn, yn, phi_out] = ang_spec_multi_prop(phi_in, lambda, delta1, deltan, z, sg.*exp(1i*phz));
    mask = circ(xn/r0l_rec, yn/r0l_rec, 1);
    mask = ones(N, N);
    phi_out_sum = phi_out_sum + phi_out;
    % accumulate realizations of the MCF
    MCF2 = MCF2 + corr2_ft(phi_out, phi_out, mask, deltan);
end
[thetan, rn] = cart2pol(xn, yn);
MCF2 = MCF2/nreals;
% collimate the beam
phi_out = phi_out_sum.* exp(-1i*pi/(lambda*R)*(xn.^2+yn.^2))/nreals;

% Plot the perturbed LG beam profile
figure;
[C,h_plot]=contourf(xn,yn,abs(phi_out),100);
colorbar
colormap('hot')
h_plot.LineStyle='none';

% Theoretical modulus of the complex degree of coherence
w = sqrt(2*p + abs(l) + 1)*wDz;
F = L*(1+(zR/L)^2);
Theta = 1 + L/F;
Lambda = 2*L/k/w^2;
sR2 = rytov;

T = 1.33*sR2*Lambda^(5/6);
q = 1.22*sR2^(6/5);
if (Theta >= 0)
    alpha = (1 - Theta^(8/3))/(1 - Theta);
else
    alpha = (1 + abs(Theta)^(8/3))/(1 - Theta);
end
MCF2_gauss = ((w0/w)^2)*exp(-T)*exp(-(Lambda*k*rn.^2)/4/L - 3*alpha*(((q*k*rn.^2)/L)^(5/6))/8);

figure; 
plot(xn(N/2+1, :), abs(MCF2_gauss(N/2+1, :))/max(abs(MCF2_gauss(N/2+1, :))), 'r--');
hold on;
plot(xn(N/2+1, :), abs(MCF2(N/2+1, :))/max(abs(MCF2(N/2+1, :))), 'go');
xlim([-D2/2 D2/2])
legend('Theoretical', 'Partial propagations')

% figure;
% [C,h_plot]=contourf(xn,yn,abs(MCF2)/max(abs(MCF2), [], 'all'),100);
% colorbar
% colormap('hot')
% h_plot.LineStyle='none';
% 
% figure;
% [C,h_plot]=contourf(xn,yn,abs(MCF2_gauss)/max(abs(MCF2_gauss), [], 'all'),100);
% colorbar
% colormap('hot')
% h_plot.LineStyle='none';
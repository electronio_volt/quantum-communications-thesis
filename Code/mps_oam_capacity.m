% ------------------------------------------
% Main code used for calculating the crosstalk matrices of 
% Laguerre-Gaussian OAM modes propagating through atmospheric turbulence
%
% Flat earth model used for "Single-Photon Orbital Angular Momentum for 
% Downlink Satellite Communications" thesis
%
% Adapted from 
% J. D. Schmidt, "Numerical Simulation of Optical Wave Propagation 
% with examples in MATLAB", SPIE Press Monograph Vol. PM199, 2010
% (Listings 9.5, 9.6, 9.7, 9.8)
% ------------------------------------------

%% Program initialization
% Add all sub-folders to path 
addpath standard_atmosphere;
addpath structure_parameter;
addpath beam_generation;
addpath numerical_propagation;
addpath detection_probability;
addpath 'detection_probability/convolution_fft';

% Clear workspace
clear;

load('r0scrn_all_h1000_Η500_min_dif.mat');

% Close previous figures
close all;

%----Parameters----%

%% Channel parameters
h0a = [1000 3000]; % Ground station altitude
h0_min = min(h0a,[],'all');
h0 = 3000;
Ha = (150:50:500)*10^3; % Satellite altitude
H_max = max(Ha,[],'all');
Ha = 150*10^3;
Cn0 = 9.6*10^(-14);
thetaza = (0:10:70);
thetaz_max = max(thetaza,[],'all');
% thetaza = 0;

L_max = (H_max-h0_min)/cosd(thetaz_max);

%% Optical parameters
lambda = 1550*10^(-9); % Optical wavelength
k = 2*pi/lambda; % Optical wavenumber
w0 = sqrt(L_max*lambda/pi); % Beamwaist radius for minimum diffraction
w0 = 15*10^(-2); % Beamwaist radius
zR = pi*(w0^2)/lambda; % Rayleigh range
p = 0; % Radial mode number
la = -4:4; % OAM quantum number
% la = 0;

if (length(thetaza) > 1)
    Pla = zeros(length(la), length(la), length(thetaza));
    Pla_max = zeros(length(la), length(la), length(thetaza));
    runs = length(thetaza);
else
    Pla = zeros(length(la), length(la), length(Ha));
    Pla_max = zeros(length(la), length(la), length(Ha));
    runs = length(Ha);
end

tic
for i = 1:length(la)
for j = 1:length(la)
for m = 1:runs

lt = la(i);
lr = la(j);
lmax = max(abs([lt lr]));

if (length(thetaza) > 1)
    thetaz = thetaza(m);
    H = Ha;
elseif (length(Ha) > 1)
    thetaz = thetaza;
    H = Ha(m);
else
    H = Ha;
    thetaz = thetaza;
end
L = (H-h0)/cosd(thetaz);

%% Geometry parameters 
r0lrms_tra = sqrt((abs(lt)+1)/2)*w0;
r0l_tra = sqrt(2)*r0lrms_tra;
wL = w0*sqrt(1+(L/zR)^2);
r0lrms_rec = sqrt((lmax+1)/2)*wL;
r0l_rec = sqrt(2)*r0lrms_rec;
D1 = 2*r0l_tra; % Diameter of the source aperture [m]
D2 = 2*r0l_rec; % Diameter of the observation aperture [m]
% D2 = 2.3;

%% NASA US Standard atmosphere calculations

h_division = 10; % Must be a multiple of ten
h = h0:h_division:H;
[temp, press, lapse_rate] = std_atmo(h0, H, h_division);

%% AFGL+WK model

S = zeros(1, length(h)); % Wind shear
Cn2 = afgl_wk(h0, H, h_division, temp, press, lapse_rate, Cn0, S);

% Calculate Cn2 from Hufnagel-Valley model (for comparison)
urms = 6;
Cn2_hv = HV_model(h0, H, h_division, urms, Cn0);

%% Sampling constraints

rF = (0.423*(k^2)*secd(thetaz)*h_division*sum(Cn2(2:end).*((h(2:end)/L).^(5/3))))^(-3/5); % Fried's parameter (Coherence radius)
rytov = 0.563*(k^(7/6))*secd(thetaz)*h_division*sum(Cn2(2:end).*((h(2:end)).^(5/6)).*((1 - h(2:end)/L).^(5/6))); % Rytov variance
rF = (0.423*(k^2)*secd(thetaz)*h_division*sum(Cn2(1:end)))^(-3/5); % Fried's parameter for plane wave
rytov = 0.563*(k^(7/6))*(secd(thetaz)^(11/6))*h_division*sum(Cn2(1:end).*((h(1:end) - h(1)).^(5/6))); % Rytov variance for plane wave

c = 2;
R = L*(1+(zR/L)^2);
% R = L;
sampl_scaling = 8;

[delta1, deltan, Nmin] = ...
    sampl_constraints(c, lambda, L, sampl_scaling, D1, D2, rF, R, 'no');

toc
N = Nmin % Number of grid points
tic
zmax = min([delta1 deltan])^2 * N /lambda;
nmin = ceil(L / zmax) + 1;
if (3*nmin<10)
    n = 10; % Number of planes
else
    n = 3*nmin;
end
% n = 3*nmin;

%% Vacuum propagation

% Create the transmitter signal
[x1, y1] = meshgrid((-N/2 : N/2-1) * delta1); % Cartesian coordinates of the transmitter plane
[theta1, r1] = cart2pol(x1, y1); % Polar coordinates of the trasnmitter plane
phi_in = oam_eigenstate(lambda, w0, lt, p, 0, r1, theta1); 
phi_in_cros = oam_eigenstate(lambda, w0, lr, p, 0, r1, theta1);

% Propagation planes positions
z = (1 : n-1) * L / (n-1);
% Propagation planes absorbing boundary
sg = exp(-(x1/(0.47*N*delta1)).^16).* exp(-(y1/(0.47*N*delta1)).^16);
t = repmat(sg, [1 1 n]);
% Propagate the beam 
[xn, yn, phi_vac] = ang_spec_multi_prop(phi_in, lambda, delta1, deltan, z, t);
[thetan, rn] = cart2pol(xn, yn);
% Collimate the beam
phi_vac = phi_vac .* exp(-1i*pi/(lambda*R)*(xn.^2+yn.^2));

%% Turbulence propagation

if N >= 512
    GPU_ENABLED = true;
else
    GPU_ENABLED = false;
end

L0 = inf; % Outer scale of turbulence
l0 = 0; % Inner scale of turbulence

zt = [0 z]; % Propagation plane locations
Delta_z = zt(2:n) - zt(1:n-1); % Propagation distances
% Grid spacings
alpha = zt / zt(n);
delta = (1-alpha) * delta1 + alpha * deltan;

% Coherence radii of propagation planes
r0scrn = phase_screen_r0(L, rF, rytov, n, lambda);

% Initialize arrays for propagated fields,
% aperture mask, and MCF
mask = circ(xn/D2, yn/D2, 1);
MCF2 = zeros(N);
MCF2_nomask = zeros(N);
sg = repmat(sg, [1 1 n]);

% Initialize array for phase screens
if GPU_ENABLED == true
    phz_gpu = gpuArray(zeros(N, N, n));
else
    phz = zeros(N, N, n);
end
nreals = 1; % Number of random realizations
% Initialize detection probability
Pl = 0;
Pl_max = 0;
Pl_gpu = 0;
Pl_max_gpu = 0;
for idxreal = 1 : nreals % Loop over realizations
    idxreal;
    if GPU_ENABLED == true
        r0scrn_gpu = gpuArray(r0scrn);
        delta_gpu = gpuArray(delta);
    end
    % Loop over screens
    for idxscr = 1 : 1 : n
        if GPU_ENABLED == true
            [phz_lo, phz_hi] = ft_sh_phase_screen(r0scrn_gpu(idxscr), N, delta_gpu(idxscr), L0, l0);
            % phz_lo = gather(phz_lo_gpu);
            % phz_hi = gather(phz_hi_gpu);
            % clear phz_lo_gpu phz_hi_gpu r0_gpu delta_gpu;
        else
            [phz_lo, phz_hi] = ft_sh_phase_screen(r0scrn(idxscr), N, delta(idxscr), L0, l0);
        end
        if GPU_ENABLED == true
            phz_gpu(:,:,idxscr) = phz_lo + phz_hi;
        else
            phz(:,:,idxscr) = phz_lo + phz_hi;
        end
        clear phz_lo phz_hi;
    end
    % Simulate turbulent propagation
    if GPU_ENABLED == true
        [xn, yn, phi_out] = ang_spec_multi_prop(phi_in, lambda, delta1, deltan, z, sg.*exp(1i*phz_gpu));
        clear phz_gpu;
    else
        [xn, yn, phi_out] = ang_spec_multi_prop(phi_in, lambda, delta1, deltan, z, sg.*exp(1i*phz));
    end
    %clear phz_gpu;
    
    % Collimate the beam
    phi_out = phi_out .* exp(-1i*pi/(lambda*R)*(xn.^2+yn.^2));
    % Accumulate realizations of the MCF
    MCF2 = MCF2 + corr2_ft(phi_out.*mask, phi_out.*mask, ones(N, N), deltan);
    MCF2_nomask = MCF2_nomask + corr2_ft(phi_out, phi_out, ones(N, N), deltan);
    % Accumulate realizations of the detection probability
    % Phi_vac is used to elliminate the effect of diffraction when
    % calculating the probability
    if GPU_ENABLED == true
        phi_vac_gpu = gpuArray(phi_vac);
        phi_out_gpu = gpuArray(phi_out);
        mask_gpu = gpuArray(mask);
        Pl_gpu = Pl_gpu + detection_prob(phi_vac_gpu, phi_out_gpu.*mask_gpu, xn(N/2+1,2) - xn(N/2+1,1), deltan);
        Pl_max_gpu = Pl_max_gpu + detection_prob(phi_vac_gpu, phi_out_gpu, xn(N/2+1,2) - xn(N/2+1,1), deltan);
        clear phi_vac_gpu phi_out_gpu mask_gpu;
    else
        Pl = Pl + detection_prob(phi_vac, phi_out.*mask, xn(N/2+1,2) - xn(N/2+1,1), deltan);
        Pl_max = Pl_max + detection_prob(phi_vac, phi_out, xn(N/2+1,2) - xn(N/2+1,1), deltan);
    end
end

if GPU_ENABLED == true
    Pl = gather(Pl_gpu);
    Pl_max = gather(Pl_max_gpu);
end

% Calculate average values of MCF, MCF without the aperture and 
% the detection probability
MCF2 = MCF2/nreals;
MCF2_nomask = MCF2_nomask/nreals;
Pl = Pl/nreals
Pl_max = Pl_max/nreals

Pla(i,j,m) = Pl;
Pla_max(i,j,m) = Pl_max;
end
end
end
toc

%% Test
Px = 1/(2*max(la) + 1);
Hx = log2(2*max(la) + 1)*ones(1, runs);
Pla_max = diag(ones(1,length(la)));
% Hxy = zeros(1, runs);
Hxy = 0;
for i = 1:length(la)
    for j = 1:length(la)
        if Pla_max(i,j)==0
            Hxy = Hxy;
        else
            Hxy = Hxy + Px*Pla_max(i,j).*(log2(Pla_max(i,j)) - log2(sum(Pla_max(:,j),1)));
        end
    end
end
% Hxy = - reshape(Hxy, [1, runs]);
Hxy = -Hxy;
C = Hx - Hxy;

%% Plots 

% Set linewidth for figures
set(0, 'DefaultLineLineWidth', 1.5);

figure;
plot(Ha*10^(-3), C);
ylim([0, 4])
xlabel('Distance $H$ [km]', 'Interpreter', 'latex')
ylabel('Capacity $C$ [bits/symbol]', 'Interpreter', 'latex')

[lgridx, lgridy] = meshgrid(la,la);
figure;
imagesc(real(Pla_max(:,:,end)))
colorbar
xlabel('Transmitted OAM Quantum Number $l_t$', 'interpreter', 'latex')
ylabel('Received OAM Quantum Number $l_r$', 'interpreter', 'latex')
title('\textbf{Crosstalk Matrix}', 'interpreter', 'latex')
set(gca,'XTickLabel',la)
set(gca,'YTickLabel',la)

% % Plot the perturbed LG beam profile in the receiver
% figure;
% [C_plot,h_plot]=contourf(xn,yn,abs(phi_out),1000);
% clb = colorbar;
% ylabel(clb, 'Intensity', 'Interpreter', 'latex');
% colormap('hot')
% h_plot.LineStyle='none';
% axis equal;
% title("\textbf{Turbulence Observational Plane Intensity ($l$ = "+ lt +")}", 'Interpreter', 'latex')
% xlabel('$x$ [m]', 'Interpreter', 'latex')
% ylabel('$y$ [m]', 'Interpreter', 'latex')

% save('crosstalk_h3000_thetaz0_reals500_w015.mat')
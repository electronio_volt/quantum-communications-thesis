function [Cn2] = HV_model(h0, H, h_division, urms, Cn0)

h = h0:h_division:H;

Cn2=0.00594*((urms/27)^2)*((h*10^(-5)).^10).*exp(-h/1000)+(2.7*10^(-16))*exp(-h/1500)+Cn0*exp(-h/100);



function [Cn2] = afgl_wk(h0, H, h_division, temp, press, lapse_rate, Cn0, S)

h = h0:h_division:H;

h_1km = 1000;
href = 10; 
gamma = 9.8; % [K/km]

if (H > h_1km && h0 < h_1km)
    [m, index_o1km] = min(abs(h-h_1km));
    if (length(index_o1km) > 1)
        index_o1km = index_o1km(1);
    end
    h_o1km = h(index_o1km + 1 : end);
    h_u1km = h(1:index_o1km);
    S_o1km = S(index_o1km + 1 : end);
    press_o1km = press(index_o1km + 1 : end);
    temp_o1km = temp(index_o1km + 1 : end);
    lapse_rate_o1km = lapse_rate(index_o1km + 1 : end);
    
    Y = zeros(1, length(h_o1km));
    for i = 1 : length(h_o1km)
        if (h_o1km(i) < 5472)
            Y(i) = 2.9767 + 27.9804 * S_o1km(i) + 2.9012 * lapse_rate_o1km(i) + ...
                1.1843 * lapse_rate_o1km(i)^2 + 0.1741 * lapse_rate_o1km(i)^3 + ...
                0.0086 * lapse_rate_o1km(i)^4;
        elseif (h_o1km(i) >= 5472 && h_o1km(i) < 14172)
            Y(i) = 0.7152 + 30.6024 * S_o1km(i) + 0.0003 * lapse_rate_o1km(i) - ...
                0.0057 * lapse_rate_o1km(i)^2 - 0.0016 * lapse_rate_o1km(i)^3 + ...
                0.0001 * lapse_rate_o1km(i)^4;
        else 
            Y(i) = 0.6763 + 8.1569 * S_o1km(i) - 0.0536 * lapse_rate_o1km(i) + ...
                0.0084 * lapse_rate_o1km(i)^2 - 0.0007 * lapse_rate_o1km(i)^3 + ...
                0.00002 * lapse_rate_o1km(i)^4;
        end
    end
    Cn2_u1km = Cn0 * (h_u1km/href).^(-2/3); % nighttime
    N2 = ((10^(-3)) * gamma * (lapse_rate_o1km + gamma)) ./ temp_o1km;
    M = (-79 * 10^(-6) * press_o1km .* N2) ./ (gamma * temp_o1km);
    Cn2_o1km = 2.8 * (M.^2) * (0.1^(4/3)) .* 10.^(Y);
    Cn2 = [Cn2_u1km Cn2_o1km];
elseif (H <= h_1km)
    Cn2 = Cn0 * (h/href).^(-2/3); % nighttime
elseif (h0 >= h_1km)
    Y = zeros(1, length(h));
    for i = 1 : length(h)
        if (h(i) < 5472)
            Y(i) = 2.9767 + 27.9804 * S(i) + 2.9012 * lapse_rate(i) + ...
                1.1843 * lapse_rate(i)^2 + 0.1741 * lapse_rate(i)^3 + ...
                0.0086 * lapse_rate(i)^4;
        elseif (h(i) >= 5472 && h(i) < 14172)
            Y(i) = 0.7152 + 30.6024 * S(i) + 0.0003 * lapse_rate(i) - ...
                0.0057 * lapse_rate(i)^2 - 0.0016 * lapse_rate(i)^3 + ...
                0.0001 * lapse_rate(i)^4;
        else 
            Y(i) = 0.6763 + 8.1569 * S(i) - 0.0536 * lapse_rate(i) + ...
                0.0084 * lapse_rate(i)^2 - 0.0007 * lapse_rate(i)^3 + ...
                0.00002 * lapse_rate(i)^4;
        end
    end
    N2 = ((10^(-3)) * gamma * (lapse_rate + gamma)) ./ temp;
    M = (-79 * 10^(-6) * press .* N2) ./ (gamma * temp);
    Cn2 = 2.8 * (M.^2) * (0.1^(4/3)) .* 10.^(Y);
end

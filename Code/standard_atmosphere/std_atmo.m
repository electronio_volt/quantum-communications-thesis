function  [temp, press, lapse_rate] = std_atmo(h0, H, h_division)

h = h0:h_division:H;

% Calculate the temperature vector
temp = zeros(1, length(h));
for i=1:length(h)
    temp(i) = atmo_temp(h(i)*10^(-3));
end

% Calculate the pressure vector
h_86km = 86000;
if (H < h_86km)
    press = 0.01*atmo_p(h*10^(-3)); 
elseif (H >= h_86km && h0 < h_86km) 
    [m, index_o86km] = min(abs(h-h_86km));
    if (length(index_o86km) > 1)
        index_o86km = index_o86km(1);
    end
    temp_o86km = temp(index_o86km : end);
    h_o86km = h(index_o86km : end);
    h_u86km = h(1 : index_o86km - 1);
    sum_n = sum(atmo_compo(H*10^(-3), h_division*10^(-3)), 2);
    press_o86km = atmo_p(h_o86km*10^(-3), temp_o86km, sum_n');
    press_u86km = atmo_p(h_u86km*10^(-3));
    press = 0.01*[press_u86km press_o86km];
elseif (h0 >= h_86km) 
    sum_n = sum(atmo_compo(H*10^(-3), h_division*10^(-3)), 2);
    press = 0.01*atmo_p(h*10^(-3), temp, sum_n');
end

% Calculate the lapse rate
lapse_rate = zeros(1,length(h));
for i=1:length(h)-1
    lapse_rate(i) = (10^3)*(temp(i+1) - temp(i))/(h(i+1) - h(i));
end
lapse_rate(end) = lapse_rate(end - 1);
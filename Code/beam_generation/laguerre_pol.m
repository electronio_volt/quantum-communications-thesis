function Lpl = laguerre_pol(l, p, x)

if (p == 0)
    Lpl = 1;
elseif (p == 1)
    Lpl = 1 - x + l;
else 
    Lpl = (2*p + l - 1 - x)*laguerre_pol(l, p - 1, x)/p - ...
        (p + l - 1)*laguerre_pol(l, p - 2, x)/p;
end
     
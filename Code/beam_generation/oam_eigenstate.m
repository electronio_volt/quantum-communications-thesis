function phi_pl = oam_eigenstate(lambda, w0, l, p, z, r, theta)
% OAM_EIGENSTATE
% Function that calculates the form of an Orbital Angular Momentum (OAM)
% eigenstate based on the radial profile of a Laguerre-Gaussian (LG) beam

Rpl = lag_gaus_beam(lambda, w0, l, p, z, r); % Radial profile of LG beam
phi_pl = exp(1j*l*theta).*Rpl/sqrt(2*pi); % OAM eigenstate


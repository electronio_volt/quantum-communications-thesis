function Rpl = lag_gaus_beam(lambda, w0, l, p, z, r)
% LAG_GAUS_BEAM
% Function that calculates the radial profile of a Laguerre-Gaussian beam

k = 2*pi/lambda; % Optical wavenumber
zR = pi*(w0^2)/lambda; % Rayleigh range
w = w0*sqrt(1+(z/zR)^2); % Beam-waist radius at distance z

% Calculate generalized Laguerre polynomial of order zero
Lpl = laguerre_pol(l, p, 2*r.^2/w^2);

% Calculate the radial profile
Rpl = 2*sqrt(factorial(p)/factorial(p+abs(l)))*(1/w).* ...
    ((r*sqrt(2)/w).^abs(l)).*exp(-r.^2/w^2)*Lpl.* ...
    exp(1j*k*r.^2*z/(2*(z^2+zR^2)))*exp(-1j*(2*p+abs(l)+1)*atan(z/zR));
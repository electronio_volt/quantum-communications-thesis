% Close previous figures
close all;

%----Parameters----%

%% Optical parameters
lambda=1550*10^(-9); % Optical wavelength
k=2*pi/lambda; % Optical wavenumber
w0=15*10^(-2); % Beamwaist radius
rt=33*10^(-2); % Transmitter aperture radius
ra=3.7; % Receiver aperture radius
zR=pi*w0^2/lambda; % Rayleigh range
p=0; % Radial mode number
Lp=1; % Generalized Laguerre polynomial of order zero
l=3; % OAM quantum number

%% Atmospheric channel parameters
urms=6; % RMS wind speed
A=9.6*10^(-14); % Hufnagel-Valley model at h=0 altitude 

%% System parameters
L=497*10^3; % Channel distance

%----Main code----%

%% LG beam construction
% Define the longitudinal plane
z=0;

% Define the cartesian coordinates
x=-ra:0.01:ra;
y=-ra:0.01:ra;

% Create a meshgrid out of the cartesian coordinates
[X,Y] = meshgrid(x,y);

% Calculate the radial coordinates
r=sqrt(X.^2+Y.^2);

% Calculate the azimuthal coordinates
theta=atan(Y./X);

% Calclulate the Laguerre-Gaussian mode radial profile R
w=w0*sqrt(1+(z/zR)^2);
R=2*sqrt(factorial(p)/factorial(p+abs(l)))*(1/w)*((r*sqrt(2)/w).^abs(l)).*exp(-r.^2/w^2)*Lp.*exp(1j*k*r.^2*z/(2*(z^2+zR^2)))*exp(-1j*(2*p+abs(l)+1)*atan(z/zR));

% Calculate the OAM eigenstate
phipl=R.*exp(1j*l*theta)/sqrt(2*pi);

% Plot the LG beam profile
figure;
% [C,h]=contourf(X,Y,R/max(max(R)),100);
[C,h]=contourf(X,Y,R,100);
colorbar
colormap('jet')
h.LineStyle='none';
title("LG pulse profile at z="+z+" for l="+l);

%% Atmoshperic turbulence effect

r0lrms=sqrt((abs(l)+1)/2)*w;
r0l=sqrt(2)*r0lrms;

W1=1.65*(sqrt(l)+1); % Elliptical semi-axis length
W2=1.6*(sqrt(l)+1); % Elliptical semi-axis length
phi=30; % Rotation angle of the beam
phi0=68; % Deviation angle of the beam centroid
r0=0.8; % Deviation radius of the beam centroid

% Calculate the new center of the beam centroid
x0=r0*cosd(phi0); 
y0=r0*sind(phi0);

% Distort the beam 
xi=r0l*x/W1;
yi=r0l*y/W2;
[Xi,Yi] = meshgrid(xi,yi);

% Rotate and change centroid position of the beam
Xi = (Xi-x0)*cosd(phi) - (Yi-y0)*sind(phi);
Yi = (Yi-y0)*cosd(phi) + (Xi-x0)*sind(phi);

% Calculate the perturbed beam profile
r=sqrt(Xi.^2+Yi.^2);
z=L;
w=w0*sqrt(1+(z/zR)^2);
R=2*sqrt(factorial(p)/factorial(p+abs(l)))*(1/w)*((r*sqrt(2)/w).^abs(l)).*exp(-r.^2/w^2)*Lp.*exp(1j*k*r.^2*z/(2*(z^2+zR^2)))*exp(-1j*(2*p+abs(l)+1)*atan(z/zR));

% Plot the perturbed LG beam profile
figure;
%[C,h]=contourf(X,Y,abs(R)/max(max(abs(R))),100);
[C,h]=contourf(X,Y,abs(R),100);
colorbar
colormap('jet')
h.LineStyle='none';
title("Perturbed LG pulse profile at z="+z+" for l="+l);

% Clear workspace
clear;

% Close previous figures
close all;

%----Parameters----%

%% Optical parameters
lambda=1550*10^(-9); % Optical wavelength
k=2*pi/lambda; % Optical wavenumber
w0=15*10^(-2); % Beamwaist radius
rt=33*10^(-2); % Transmitter aperture radius
ra=3.7; % Receiver aperture radius
zR=pi*(w0^2)/lambda; % Rayleigh range
p=0; % Radial mode number
Lp=1; % Generalized Laguerre polynomial of order zero
la=[0 1 2 3 4]; % OAM quantum number
lra=la;

%% Atmospheric channel parameters
urms=6; % RMS wind speed
A=9.6*10^(-14); % Hufnagel-Valley model at h=0 altitude 

%% System parameters
h0a=[0 1000 3000]; % Ground station altitude
Ha=(150:50:500)*10^3; % Satellite altitude
thetaz=0; % Zenith angle
sims=2; % Number of realizations of the random variables

%----Main code----%

%% Coordinate systems construction

% Calculate the radial coordinate
dr=0.05;
r=0:dr:ra;

% Calculate the azimuthal coordinate
dtheta=2*pi/(length(r)-1);
theta=0:dtheta:2*pi;

% Calculate the corresponding cartesian coordinates
x=r.*cos(theta);
y=r.*sin(theta);

% Create meshgrid out of the polar coordinates
[rm,thetam,thetama] = meshgrid(r,theta,theta);

%% Atmoshperic turbulence effect

avPl=zeros(length(h0a)*length(la),length(Ha));

for u=1:length(h0a)
    h0=h0a(u);
    for m=1:length(la)
        l=la(m);
        lr=lra(m);
        for j=1:length(Ha)
            H=Ha(j);
            
            % Calculate the channel distance
            L=(H-h0)/cos(thetaz);
            
            % Define the longitudinal plane (receiver)
            z=L;
            w=w0*sqrt(1+(z/zR)^2);
            
            % Calculate the RMS beam radius
            r0lrms=sqrt((abs(l)+1)/2)*w;
            
            % Calculate the quantity within most (~90%) of the optical power is
            % distributed
            r0l=sqrt(2)*r0lrms; 
            
            % Calculate the realizations of the random variables
            Omega=k*(w0^2)/2/L;
            Cn2 = @(h) 0.00594*((urms/27)^2)*((h*10^(-5)).^10).*exp(-h/1000)+(2.7*10^(-16))*exp(-h/1500)+A*exp(-h/100);
            fun = @(h) (0.00594*((urms/27)^2)*((h*10^(-5)).^10).*exp(-h/1000)+(2.7*10^(-16))*exp(-h/1500)+A*exp(-h/100)).*(h-h0).^(5/6);
            sigmaR2=2.25*(k^(7/6))*(sec(thetaz)^(11/6))*integral(fun,h0,H); % Rytov variance
            sigmaI2=exp((0.49*sigmaR2/((1+1.11*sigmaR2^(12/5))^(7/6)))+(0.51*sigmaR2/((1+0.69*sigmaR2^(12/5))^(5/6))))-1; %Scintillation index
            meanTheta=log(((1+2.96*sigmaI2*Omega^(5/6))^2)/((Omega^2)*sqrt((1+2.96*sigmaI2*Omega^(5/6))^2+1.2*sigmaI2*Omega^(5/6))));
            varTheta=log(1+(1.2*sigmaI2*Omega^(5/6))/((1+2.96*sigmaI2*Omega^(5/6))^2));
            corTheta=log(1-(0.8*sigmaI2*Omega^(5/6))/((1+2.96*sigmaI2*Omega^(5/6))^2));
            varc0=0.33*(w0^2)*sigmaI2*Omega^(-7/6);
            M=[varc0 0 0 0;0 varc0 0 0;0 0 varTheta corTheta;0 0 corTheta varTheta];
            mean=[0 0 meanTheta meanTheta];
            B=mvnrnd(mean,M,sims);

            x0=B(:,1);
            y0=B(:,2);
            Theta1=B(:,3);
            Theta2=B(:,4);
            W1=sqrt(exp(Theta1)*w0^2)*sqrt(abs(l)+1);
            W2=sqrt(exp(Theta2)*w0^2)*sqrt(abs(l)+1);
            phi=unifrnd(0,pi/2,1,sims);
            
            % Calculate the Fried parameter
            rF=(0.423*(k^2)*sec(thetaz)*integral(Cn2,h0,H))^(-3/5); 

            % Calculate the symmetrical circular aperture function
            Hr=heaviside(ra-r);
            
            % Calculate the ensemble average of the phase profile
            Ca=exp(-6.88*(2^(2/3))*((rm/rF).^(5/3)).*abs(sin((thetam-thetama)/2)).^(5/3));

            Pl=zeros(1,sims);
            for i=1:sims    
                % Change centroid position of the beam
                xt=x-x0(i);
                yt=y-y0(i);
                
                % Calculate the beam shape matrix
                S=[W1(i)*cos(phi(i)) -W2(i)*sin(phi(i));W1(i)*sin(phi(i)) W2(i)*cos(phi(i))]/r0l;
                
                % Distort the beam 
                ci=S\[xt; yt];
                xi=ci(1,:);
                yi=ci(2,:);
                
                % Calculate the perturbed polar coordinates
                ri=sqrt(xi.^2+yi.^2);
                thetai=atan2(yi,xi);

                % Create meshgrid out of the perturbed polar coordinates
                [rim,thetaim,thetaima] = meshgrid(ri,thetai,thetai);

                % Calclulate the perturbed radial profile
                Ri=2*sqrt(factorial(p)/factorial(p+abs(l)))*(1/w)*((ri*sqrt(2)/w).^abs(l)).*exp(-ri.^2/w^2)*Lp.*exp(1j*k*ri.^2*z/(2*(z^2+zR^2)))*exp(-1j*(2*p+abs(l)+1)*atan(z/zR));

                % Calculate the detection propability
                Thetar1=sum(Ca.*exp(-1i*l*thetaima).*exp(1i*lr*thetama),3)*dtheta;
                Thetar2=sum(Thetar1.*exp(1i*l*thetaim(:,:,1)).*exp(-1i*lr*thetam(:,:,1)))*dtheta;
                Pl(i)=sum(((abs(Ri).*abs(Hr)).^2).*(Thetar2.*r))*dr/det(S)/4/(pi^2);

            end
            avPl(m+(u-1)*length(la),j)=sum(Pl)/sims;
        end
    end
end

%% Plot the results
figure;
semilogy(Ha*10^(-3),avPl(2*length(la)+1:3*length(la),:));
hold on;
set(gca,'ColorOrderIndex',1)
semilogy(Ha*10^(-3),avPl(1:length(la),:),'*-');
hold on;
set(gca,'ColorOrderIndex',1)
semilogy(Ha*10^(-3),avPl(length(la)+1:2*length(la),:),'--');
ylim([0.001 1])
xlim([150 500])
xlabel('Satellite altitude $H (km)$','Interpreter','latex')
ylabel('Detection Probability $P(l_0)$','Interpreter','latex')
legend('$l_0=0$','$l_0=$1','$l_0=2$','$l_0=3$','$l_0=4$','Interpreter','latex')
% title("OAM detection probabilities at %\lambda=%"+lambda*10^9+" nm","Interpreter","latex")
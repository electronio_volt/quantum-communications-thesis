% ------------------------------------------
% Main code used for calculating the detection probability
% of Laguerre-Gaussian OAM modes propagating through 
% atmospheric turbulence
%
% Keplerian elements based model with satellite position extracted from 
% the General Mission Analysis Tool (GMAT)
%
% Adapted from 
% J. D. Schmidt, "Numerical Simulation of Optical Wave Propagation 
% with examples in MATLAB", SPIE Press Monograph Vol. PM199, 2010
% (Listings 9.5, 9.6, 9.7, 9.8)
% ------------------------------------------

%% Program initialization
% Add all sub-folders to path 
addpath standard_atmosphere;
addpath structure_parameter;
addpath beam_generation;
addpath numerical_propagation;
addpath detection_probability;
addpath 'detection_probability/convolution_fft';

% % Clear workspace
% clear;
% 
% % Close previous figures
% close all;

%----GMAT input----%

% opts = detectImportOptions('TestReport.txt');
% opts = setvaropts(opts, ...
%                        'DatetimeFormat','dd MMM yyyy HH:mm:ss.SSS',...
%                        'FillValue','now');
% A = readtable('TestReport.txt', 'Format','%17s%26s%26s%26s%11s%48s%[^\n\r]');

A = importfile('TestReport.txt', 2, inf);
eleva = table2array(A(:,1));
La = table2array(A(:,2));
timea = table2array(A(:,3));
Ha = table2array(A(:,4));
h0a = table2array(A(:,5));
utc_timea = table2array(A(:,6));

%----Parameters----%

%% Channel parameters

Cn0 = 9.6*10^(-14);
RE = 6371*10^3;

L_max = max(La);

%% Optical parameters
lambda = 1550*10^(-9); % Optical wavelength
k = 2*pi/lambda; % Optical wavenumber
w0 = sqrt(L_max*lambda/pi); % Beamwaist radius for minimum diffraction
w0 = 15*10^(-2); % Beamwaist radius
zR = pi*(w0^2)/lambda; % Rayleigh range
p = 0; % Radial mode number
la = [0 1 2 3 4]; % OAM quantum number
la = 3;

Pla = zeros(length(la), length(La));
Pla_max = zeros(length(la), length(La));
runs = length(La);

tic
for i = 1:length(la)
for j = 57:57

l = la(i);
thetaz = 90 - eleva(j);
L = La(j);
H = Ha(j);
h0 = h0a(j);

%% Geometry parameters 
r0lrms_tra = sqrt((l+1)/2)*w0;
r0l_tra = sqrt(2)*r0lrms_tra;
wL = w0*sqrt(1+(L/zR)^2);
r0lrms_rec = sqrt((l+1)/2)*wL;
r0l_rec = sqrt(2)*r0lrms_rec;
D1 = 2*r0l_tra; % Diameter of the source aperture [m]
D2 = 2*r0l_rec; % Diameter of the observation aperture [m]
D2 = 2.3;
r0l_0 = sqrt((l+1)/2)*w0;
r0l_L = sqrt((l+1)/2)*wL;

%% NASA US Standard atmosphere calculations

h_division = 100; % Must be a multiple of ten
h = h0:h_division:H;
[temp, press, lapse_rate] = std_atmo(h0, H, h_division);

%% AFGL+WK model

S = zeros(1, length(h)); % Wind shear
Cn2 = afgl_wk(h0, H, h_division, temp, press, lapse_rate, Cn0, S);

% Calculate Cn2 from Hufnagel-Valley model (for comparison)
urms = 6;
Cn2_hv = HV_model(h0, H, h_division, urms, Cn0);

%% Sampling constraints

rF = (0.423*(k^2)*h_division*sum(Cn2(1:end).*((h(1:end) + RE)./sqrt((h(1:end) + RE).^2 - (RE + h0)^2*sind(thetaz)^2))))^(-3/5); % Fried's parameter for plane wave
rytov = 0.563*(k^(7/6))*h_division*sum(Cn2(1:end).*((h(1:end) + RE)./sqrt((h(1:end) + RE).^2 - (RE + h0)^2*sind(thetaz)^2)).*((h(1:end) - h(1)).^(5/6)));

c = 1;
R = L*(1+(zR/L)^2);
sampl_scaling = 8;

[delta1, deltan, Nmin] = ...
    sampl_constraints(c, lambda, L, sampl_scaling, D1, D2, rF, R, 'no');

toc
N = Nmin % Number of grid points
tic
zmax = min([delta1 deltan])^2 * N /lambda;
nmin = ceil(L / zmax) + 1;
if (3*nmin<10)
    n = 10; % Number of planes
else
    n = 3*nmin;
end
% n = 3*nmin;

%% Vacuum propagation

% Create the transmitter signal
[x1, y1] = meshgrid((-N/2 : N/2-1) * delta1); % Cartesian coordinates of the transmitter plane
[theta1, r1] = cart2pol(x1, y1); % Polar coordinates of the trasnmitter plane
phi_in = oam_eigenstate(lambda, w0, l, p, 0, r1, theta1); 

% Propagation planes positions
z = (1 : n-1) * L / (n-1);
% Propagation planes absorbing boundary
sg = exp(-(x1/(0.47*N*delta1)).^16).* exp(-(y1/(0.47*N*delta1)).^16);
t = repmat(sg, [1 1 n]);
% Propagate the beam 
[xn, yn, phi_vac] = ang_spec_multi_prop(phi_in, lambda, delta1, deltan, z, t);
[thetan, rn] = cart2pol(xn, yn);
% Collimate the beam
phi_vac = oam_eigenstate(lambda, w0, l, p, L, r1, theta1); 
% phi_vac = phi_vac .* exp(-1i*pi/(lambda*R)*(xn.^2+yn.^2));
% phi_vac = oam_eigenstate(lambda, w0, l, p, L, r1, theta1); 

%% Turbulence propagation

if N >= 1024
    GPU_ENABLED = true;
else
    GPU_ENABLED = false;
end

L0 = inf; % Outer scale of turbulence
l0 = 0; % Inner scale of turbulence

zt = [0 z]; % Propagation plane locations
Delta_z = zt(2:n) - zt(1:n-1); % Propagation distances
% Grid spacings
alpha = zt / zt(n);
delta = (1-alpha) * delta1 + alpha * deltan;

% Coherence radii of propagation planes
r0scrn = phase_screen_r0(L, rF, rytov, n, lambda);

% Initialize arrays for propagated fields,
% aperture mask, and MCF
mask = circ(xn/D2, yn/D2, 1);
MCF2 = zeros(N);
MCF2_nomask = zeros(N);
sg = repmat(sg, [1 1 n]);

% Initialize array for phase screens
if GPU_ENABLED == true
    phz_gpu = gpuArray(zeros(N, N, n));
else
    phz = zeros(N, N, n);
end
nreals = 1; % Number of random realizations
% Initialize detection probability
Pl = 0;
Pl_max = 0;
Pl_gpu = 0;
Pl_max_gpu = 0;
for idxreal = 1 : nreals % Loop over realizations
    idxreal;
    if GPU_ENABLED == true
        r0scrn_gpu = gpuArray(r0scrn);
        delta_gpu = gpuArray(delta);
    end
    % Loop over screens
    for idxscr = 1 : 1 : n
        if GPU_ENABLED == true
            [phz_lo, phz_hi] = ft_sh_phase_screen(r0scrn_gpu(idxscr), N, delta_gpu(idxscr), L0, l0);
            % phz_lo = gather(phz_lo_gpu);
            % phz_hi = gather(phz_hi_gpu);
            % clear phz_lo_gpu phz_hi_gpu r0_gpu delta_gpu;
        else
            [phz_lo, phz_hi] = ft_sh_phase_screen(r0scrn(idxscr), N, delta(idxscr), L0, l0);
        end
        if GPU_ENABLED == true
            phz_gpu(:,:,idxscr) = phz_lo + phz_hi;
        else
            phz(:,:,idxscr) = phz_lo + phz_hi;
        end
        clear phz_lo phz_hi;
    end
    % Simulate turbulent propagation
    if GPU_ENABLED == true
        [xn, yn, phi_out] = ang_spec_multi_prop(phi_in, lambda, delta1, deltan, z, sg.*exp(1i*phz_gpu));
        clear phz_gpu;
    else
        [xn, yn, phi_out] = ang_spec_multi_prop(phi_in, lambda, delta1, deltan, z, sg.*exp(1i*phz));
    end
    %clear phz_gpu;
    
    % Collimate the beam
    phi_out = phi_out .* exp(-1i*pi/(lambda*R)*(xn.^2+yn.^2));
    % Accumulate realizations of the MCF
    MCF2 = MCF2 + corr2_ft(phi_out.*mask, phi_out.*mask, ones(N, N), deltan);
    MCF2_nomask = MCF2_nomask + corr2_ft(phi_out, phi_out, ones(N, N), deltan);
    % Accumulate realizations of the detection probability
    % Phi_vac is used to elliminate the effect of diffraction when
    % calculating the probability
    if GPU_ENABLED == true
        phi_vac_gpu = gpuArray(phi_vac);
        phi_out_gpu = gpuArray(phi_out);
        mask_gpu = gpuArray(mask);
        Pl_gpu = Pl_gpu + detection_prob(phi_vac_gpu, phi_out_gpu.*mask_gpu, xn(N/2+1,2) - xn(N/2+1,1), deltan);
        Pl_max_gpu = Pl_max_gpu + detection_prob(phi_vac_gpu, phi_out_gpu, xn(N/2+1,2) - xn(N/2+1,1), deltan);
        clear phi_vac_gpu phi_out_gpu mask_gpu;
    else
        Pl = Pl + detection_prob(phi_vac, phi_out.*mask, xn(N/2+1,2) - xn(N/2+1,1), deltan);
        Pl_max = Pl_max + detection_prob(phi_vac, phi_out, xn(N/2+1,2) - xn(N/2+1,1), deltan);
    end
end

if GPU_ENABLED == true
    Pl = gather(Pl_gpu);
    Pl_max = gather(Pl_max_gpu);
end

% Calculate average values of MCF, MCF without the aperture and 
% the detection probability
MCF2 = MCF2/nreals;
MCF2_nomask = MCF2_nomask/nreals;
Pl = Pl/nreals
Pl_max = Pl_max/nreals

Pla(i,j) = Pl;
Pla_max(i,j) = Pl_max;

end
end
toc

%% Plots 

% Set linewidth for figures
set(0, 'DefaultLineLineWidth', 1.5);
% 
% % hold on;
% set(gca,'ColorOrderIndex',1)
% if (size(Pla_max, 2) > 1)
% figure;
% Legend = cell(length(la),1);
% interp = 2; % Interpolation factor
% for i = 1:length(la)
%     timea_smooth = linspace(timea(1), timea(end), length(timea)*interp - 1);
% %   Plot probability vs time
%     Pla_max_smooth = spline(timea, real(Pla_max(i,:)), timea_smooth);
%     semilogy(timea_smooth, smooth(smooth(Pla_max_smooth)),'--');
%     xlabel('Time (s)', 'Interpreter', 'latex')
%     xlim([timea(1) timea(end)])
%     ylim([10^-3 1])
%     ylabel('Detection Probability $P(l)$', 'Interpreter', 'latex')
%     if (length(la) > 1)
%         title("\textbf{Detection Probability}", 'Interpreter', 'latex')
%         Legend{i} = "$l =$ " + la(i);
%     else
%         title("\textbf{Detection Probability ($l$ = "+ la(i) +")}", 'Interpreter', 'latex')
%     end  
% hold on;    
% end
% if (length(la) > 1)
%     hl = legend(Legend);
%     set(hl ,'Interpreter','latex')
% end
% hold off;
% end

% % hold on;
% figure;
% plot(thetaza,rytov);
% xlim([thetaza(1) thetaza(end)])
% title('Rytov variance')
% 
% rytov2 = rytov*4;
% scint = exp((0.49*rytov2./(1+1.11*rytov2.^(6/5)).^(7/6))+(0.51*rytov2./(1+0.69*rytov2.^(6/5))).^(5/6)) - 1;
% % scint = exp(4*rytov) - 1;
% hold on;
% % figure;
% plot(thetaza,scint);
% xlim([thetaza(1) thetaza(end)])
% title('Scintillation index')

% hold on;
% % figure;
% plot(thetaza,rF);
% xlim([thetaza(1) thetaza(end)])
% title('Coherence diameter')

% % % Plot Standard Atmosphere pressure, temperature and lapse rate
% % figure;
% % plot(h*10^(-3), press);
% % title('\textbf{U.S. Standard Atmosphere model}', 'Interpreter', 'latex')
% % xlabel('Altitude [km]', 'Interpreter', 'latex')
% % ylabel('Pressure [mbar]', 'Interpreter', 'latex')
% % grid on;
% % figure;
% % plot(h*10^(-3), temp);
% % title('\textbf{U.S. Standard Atmosphere model}', 'Interpreter', 'latex')
% % xlabel('Altitude [km]', 'Interpreter', 'latex')
% % ylabel('Temperature [K]', 'Interpreter', 'latex')
% % grid on;
% % figure;
% % plot(h*10^(-3), lapse_rate)
% % title('\textbf{U.S. Standard Atmosphere model}', 'Interpreter', 'latex')
% % xlabel('Altitude [km]', 'Interpreter', 'latex')
% % ylabel('Lapse Rate [K/km]', 'Interpreter', 'latex')
% % grid on;
% % 
% % % Plot the structure parameter
% % figure; 
% % semilogx(Cn2, h*10^(-3));
% % hold on;
% % semilogx(Cn2_hv, h*10^(-3));
% % xlabel('$C_n^2$ [$\mathrm{m}^{-2/3}$]', 'Interpreter', 'latex')
% % ylabel('Altitude [km]', 'Interpreter', 'latex')
% % legend('Standard Atmosphere', 'Hufnagel-Valley', 'Interpreter', 'latex')
% % title('\textbf{Structure Parameter Model}', 'Interpreter', 'latex')
% % xlim([10^(-22) 10^(-13)])
% % ylim([0 35])
% % grid on;
% 
% % Plot received intensity for verifying vacuum propagation with theoretical
% % irradiance
% % Theoretical propagated beam in vacuum
% phi_vac_theor = oam_eigenstate(lambda, w0, l, p, L, rn, thetan);
% figure;
% plot(xn(N/2+1,:), abs(phi_vac_theor(N/2+1,:)));
% hold on; 
% plot(xn(N/2+1,:), abs(phi_vac(N/2+1,:)),'o');
% xlim([xn(N/2+1,1) xn(N/2+1,end)])
% legend('Theoretical', 'Partial Propagations', 'Interpreter', 'latex')
% title("\textbf{Free-Space Observational Plane Intensity ($l$ = "+ l +")}", 'Interpreter', 'latex')
% ylabel('Intensity','Interpreter','latex')
% xlabel('$x$ [m]','Interpreter','latex')
% grid on;
% 
% % Plot the LG beam profile at the transmitter
% figure;
% [C,h_plot]=contourf(x1,y1,abs(phi_in),1000);
% clb = colorbar;
% ylabel(clb, 'Intensity', 'Interpreter', 'latex');
% colormap('hot')
% h_plot.LineStyle='none';
% title("\textbf{Source Plane Intensity ($l$ = "+ l +")}", 'Interpreter', 'latex')
% xlabel('$x$ [m]', 'Interpreter', 'latex')
% ylabel('$y$ [m]', 'Interpreter', 'latex')
% axis equal;
% % xlim([-D2 D2])
% % ylim([-D2 D2])

% % Plot the LG beam phase at the transmitter
% figure;
% [C,h_plot]=contourf(x1,y1,angle(phi_in),1000);
% clb = colorbar;
% ylabel(clb, 'Phase [rad]', 'Interpreter', 'latex');
% colormap('gray')
% h_plot.LineStyle='none';
% title("\textbf{Source Plane Phase ($l$ = "+ l +")}", 'Interpreter', 'latex')
% xlabel('$x$ [m]', 'Interpreter', 'latex')
% ylabel('$y$ [m]', 'Interpreter', 'latex')
% axis equal;
% % xlim([-D2 D2])
% % ylim([-D2 D2])

% 
% % Verify vacuum propagation result through the theoretical modulus of the 
% % complex degree of coherence
% if (l == 0) % Gaussian beam
%     sR2 = 0; % Rytov variance in vacuum
%     % Theoretical MCF for gaussian beam
%     MCF2_gauss_vac = gaussian_MCF2(L, rn, p, l, w0, sR2, lambda);
%     % Numerical MCF for gaussian beam
%     MCF2_vac = corr2_ft(phi_vac, phi_vac, ones(N, N), deltan);
%     
%     % Plot comparison of theoretical and calculated values
%     figure; 
%     plot(xn(N/2+1, :), abs(MCF2_gauss_vac(N/2+1, :))/max(abs(MCF2_gauss_vac(N/2+1, :))));
%     hold on;
%     plot(xn(N/2+1, :), abs(MCF2_vac(N/2+1, :))/max(abs(MCF2_vac(N/2+1, :))), 'o');
%     xlim([xn(N/2+1,1) xn(N/2+1,end)])
%     legend('Theoretical', 'Partial Propagations', 'Interpreter', 'latex')
%     title('\textbf{Free-Space Observational Plane Coherence Factor}', 'Interpreter', 'latex')
%     ylabel('Coherence Factor','Interpreter','latex')
%     xlabel('$x$ [m]','Interpreter','latex')
%     grid on;
% end
% 
% % Verify turbulence propagation result through the theoretical modulus of 
% % the complex degree of coherence
% if (l == 0) % Gaussian beam
%     sR2 = rytov;
% %     MCF2_gauss = gaussian_MCF2(L, rn, p, l, w0, sR2, lambda);
%     MCF2_gauss = exp(-6.88*(rn/rF).^(5/3)/2);
% %     MCF2_gauss = exp(-1.46*k^2*rn.^(5/3)*h_division*sum(Cn2(1:end))); % Gives same result as the previous equation
%     
%     % Plot comparison of theoretical and calculated values
%     figure; 
%     plot(xn(N/2+1, :), abs(MCF2_gauss(N/2+1, :))/max(abs(MCF2_gauss(N/2+1, :))));
%     hold on;
%     plot(xn(N/2+1, :), abs(MCF2_nomask(N/2+1, :))/max(abs(MCF2_nomask(N/2+1, :))), 'o');
%     xlim([xn(N/2+1,1) xn(N/2+1,end)])
%     legend('Theoretical', 'Partial Propagations', 'Interpreter', 'latex')
%     title("\textbf{Turbulence Observational Plane Coherence Factor ($n_{real}$ = "+ nreals +")}", 'Interpreter', 'latex')
%     ylabel('Coherence Factor','Interpreter','latex')
%     xlabel('$x$ [m]','Interpreter','latex')
%     grid on;
% end
% 
% % Plot the perturbed LG beam profile in the receiver
% figure;
% [C,h_plot]=contourf(xn,yn,abs(phi_out),1000);
% clb = colorbar;
% ylabel(clb, 'Intensity', 'Interpreter', 'latex');
% colormap('hot')
% h_plot.LineStyle='none';
% axis equal;
% title("\textbf{Turbulence Observational Plane Intensity ($l$ = "+ l +")}", 'Interpreter', 'latex')
% xlabel('$x$ [m]', 'Interpreter', 'latex')
% ylabel('$y$ [m]', 'Interpreter', 'latex')
% % xlim([-D2 D2])
% % ylim([-D2 D2])
% 
% % Make inner and outer boundaries
% t = linspace(0,2*pi);
% rin = D2/2;
% rout = 1.01*rin;
% xin = rin*cos(t);
% xout = rout*cos(t);
% yin = rin*sin(t);
% yout = rout*sin(t);
% % Make patch
% hp = patch([xout,xin],[yout,yin],'w','linestyle','none');
% hl1 = line(xin,yin,'color',[150 150 150]/255);
% hl2 = line(xout,yout,'color',[150 150 150]/255);
% axis equal
% 
% % Plot the LG beam phase at the receiver
% figure;
% [C,h_plot]=contourf(x1,y1,angle(phi_vac),1000);
% clb = colorbar;
% ylabel(clb, 'Phase [rad]', 'Interpreter', 'latex');
% colormap('gray')
% h_plot.LineStyle='none';
% title("\textbf{Source Plane Phase ($l$ = "+ l +")}", 'Interpreter', 'latex')
% xlabel('$x$ [m]', 'Interpreter', 'latex')
% ylabel('$y$ [m]', 'Interpreter', 'latex')
% axis equal;
% % xlim([-D2 D2])
% % ylim([-D2 D2])

% % Plot a phase screen sample
% figure;
% [C,h_plot] = contourf(xn, yn, phz(:,:,1), 1000);
% clb = colorbar;
% ylabel(clb, 'Phase [rad]', 'Interpreter', 'latex');
% colormap('gray')
% h_plot.LineStyle='none';
% xlabel('x [m]','Interpreter','latex')
% ylabel('y [m]','Interpreter','latex')
% title('\textbf{Random Phase Screen Realization}', 'Interpreter', 'latex')
% axis equal;

%% Test section

dt = timea(2) - timea(1);
counter1 = 1;
counter2 = 1;
for i = 2:runs
    if (timea(i) - timea(i - 1) > 2*dt)
        passDurEnda(counter2) = i - 1;
        passDurIndexa(counter2) = counter1;
        counter2 = counter2 + 1
        counter1 = 1;
        passDurStarta(counter2) = i;
    elseif (i == 2)
        passDurStarta(1) = i - 1;
        counter1 = counter1 + 1;
    elseif (i == runs)
        passDurIndexa(counter2) = counter1 + 1;
        passDurEnda(counter2) = i;
    else
        counter1 = counter1 + 1;
    end
end

figure;
Legend = cell(length(passDurIndexa),1);
index_start = 1;
index_end = passDurIndexa(1);
interp = 2; % Interpolation factor
for i = 1:length(passDurIndexa) 
    passDur = 0:dt:(passDurIndexa(i) - 1)*dt;
    passDur_smooth = linspace(passDur(1), passDur(end), length(passDur)*interp - 1);
    % Plot probability vs time
    Pla_max_smooth = spline(passDur, real(Pla(1,index_start:index_end)), passDur_smooth);
    semilogy(passDur_smooth, Pla_max_smooth)
    Legend{i} = datestr(utc_timea(index_start));
    index_start = index_end + 1;
    if i < length(passDurIndexa) 
        index_end = index_end + passDurIndexa(i + 1);
        hold on;
    end
end
hl = legend(Legend);
xlabel('Pass Duration (s)', 'Interpreter', 'latex')
[passDurIndexMax,index] = max(passDurIndexa);
passDurMax = 0:dt:(passDurIndexMax - 1)*dt;
xlim([passDurMax(1) passDurMax(end)])
ylim([10^(-4) 1])
ylabel('Detection Probability $P(l)$', 'Interpreter', 'latex')
title("\textbf{Detection Probability ($l$ = 0)}", 'Interpreter', 'latex')

figure;
ax = axes;
ax.ColorOrder = [0.3010 0.7450 0.9330];
ax.LineStyleOrder = {'-','--','-.','-*','-o'};
set(gca, 'YScale', 'log')
Legend2 = cell(length(la),1); 
hold on;
for i = 1:length(la)
    semilogy(passDurMax, Pla(i,passDurStarta(index):passDurEnda(index)))
    if (length(la) > 1)
        title("\textbf{Detection Probability} ("+datestr(utc_timea(passDurStarta(index)))+")", 'Interpreter', 'latex')
        Legend2{i} = "$l =$ " + la(i);
    else
        title("\textbf{Detection Probability ($l$ = "+ la(i) +")}", 'Interpreter', 'latex')
    end 
end
hold off;
if (length(la) > 1)
    hl = legend(Legend2);
    set(hl ,'Interpreter','latex')
end
% ylim([10^(-4) 1])
xlim([passDurMax(1) passDurMax(end)])
xlabel('Pass Duration (s)', 'Interpreter', 'latex')
ylabel('Detection Probability $P(l)$', 'Interpreter', 'latex')
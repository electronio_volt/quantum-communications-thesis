function phz = ft_phase_screen(r0, N, delta, L0, l0)

% ------------------------------------------
% Function for phase screen generation based on random draws of turbulence
% through the FT method
%
% Retrieved on December 2020 by
% J. D. Schmidt, "Numerical Simulation of Optical Wave Propagation 
% with examples in MATLAB", SPIE Press Monograph Vol. PM199, 2010
% (Listing 9.2)
% ------------------------------------------

% setup the PSD
del_f = 1/(N*delta); % frequency grid spacing [1/m]
fx = (-N/2 : N/2-1) * del_f;
% frequency grid [1/m]
[fx, fy] = meshgrid(fx);
[th, f] = cart2pol(fx, fy); % polar grid
fm = 5.92/l0/(2*pi); % inner scale frequency [1/m]
f0 = 1/L0; % outer scale frequency [1/m]
% modified von Karman atmospheric phase PSD
PSD_phi = 0.023*r0^(-5/3) * exp(-(f/fm).^2) ./ (f.^2 + f0^2).^(11/6);
PSD_phi(N/2+1,N/2+1) = 0;
% random draws of Fourier coefficients
cn = (randn(N) + 1i*randn(N)) .* sqrt(PSD_phi)*del_f;
% synthesize the phase screen
phz = real(ift2(cn, 1));
clear cn PSD_phi fm f0 th f fx fy;
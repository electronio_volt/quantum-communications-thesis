function z = circ(x, y, D)

% ------------------------------------------
% Function for circular aperture creation
% 
% Retrieved on December 2020 by
% J. D. Schmidt, "Numerical Simulation of Optical Wave Propagation 
% with examples in MATLAB", SPIE Press Monograph Vol. PM199, 2010
% (Listing B.3)
% ------------------------------------------

r = sqrt(x.^2+y.^2);
z = double(r<D/2);
z(r==D/2) = 0.5;
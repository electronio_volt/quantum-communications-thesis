function G = ft2(g, delta)

% ------------------------------------------
% Function for calculating 2D DFT
%
% Retrieved on December 2020 by
% J. D. Schmidt, "Numerical Simulation of Optical Wave Propagation 
% with examples in MATLAB", SPIE Press Monograph Vol. PM199, 2010
% (Listing 2.5)
% ------------------------------------------

G = fftshift(fft2(fftshift(g))) * delta^2;
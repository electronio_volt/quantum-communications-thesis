function [delta1, deltan, Nmin] = ... 
    sampl_constraints(c, lambda, z, sampl_scaling, tra_aper, ... 
    rec_aper, rF, R, enable_plot) 

% ------------------------------------------
% Function for calculating the sampling constraints of the MPS method
%
% Adapted from 
% J. D. Schmidt, "Numerical Simulation of Optical Wave Propagation 
% with examples in MATLAB", SPIE Press Monograph Vol. PM199, 2010
% (Listing 9.6)
% ------------------------------------------

D1p = tra_aper + c*lambda*z/rF;
D2p = rec_aper + c*lambda*z/rF;

delta1 = lambda*z/(D1p + D2p);
deltan = delta1;
Nmin_start = 2^(ceil(log2(D1p/2/delta1 + D2p/2/deltan + lambda*z/2/delta1/deltan)));
if (delta1 > tra_aper/sampl_scaling)
    delta1 = tra_aper/sampl_scaling;
    deltan = delta1;
    Nmin = 2^(ceil(log2(D1p/2/delta1 + D2p/2/deltan + lambda*z/2/delta1/deltan)));
    while (Nmin > 2*Nmin_start || Nmin >= 1024)
        if (sampl_scaling == 1)
            error('Cannot have sampl_scaling smaller than 1. Please use different sampling parameters.')
        else
            sampl_scaling = sampl_scaling - 1;
        end
        delta1 = tra_aper/sampl_scaling;
        deltan = delta1;
        Nmin = 2^(ceil(log2(D1p/2/delta1 + D2p/2/deltan + lambda*z/2/delta1/deltan)));
    end
else
    Nmin = Nmin_start;
end

if (strcmpi(enable_plot, 'yes')) 
    
    delta1_vec = linspace(0, 1.1*lambda*z/D2p, 100);
    deltan_vec = linspace(0, 1.1*lambda*z/D1p, 100);

    % Constraint 1
    deltan_max = -D2p/D1p*delta1_vec + lambda*z/D1p;
    % Constraint 3
    deltan_min3 = (1+z/R)*delta1_vec - lambda*z/D1p;
    deltan_max3 = (1+z/R)*delta1_vec + lambda*z/D1p;
    [delta1c, deltanc] = meshgrid(delta1_vec, deltan_vec);
    % Constraint 2
    N2 = (lambda * z + D1p*deltanc + D2p*delta1c) ./ (2 * delta1c .* deltanc);

    % Plot contour of constraints
    figure;
    [C,h_plot] = contourf(delta1c*10^3,deltanc*10^3,log2(N2),10);
    colorbar
    colormap(flipud(gray))
    h_plot.LineStyle='none';
    hold on; 
    plot(delta1_vec*10^3, deltan_max*10^3, 'k--', 'Linewidth', 1.2);
    hold on;
    plot(delta1_vec*10^3, deltan_max3*10^3, 'k-.', 'Linewidth', 1.2);
    hold on;
    plot(delta1_vec*10^3, deltan_min3*10^3, 'k-.', 'Linewidth', 1.2);
    hold on; 
    plot(delta1_vec*10^3, delta1_vec*10^3, 'k:', 'Linewidth', 1.2);
    hold on;
    plot(delta1*10^3,deltan*10^3,'r*')
    xlim([delta1_vec(1) delta1_vec(end)]*10^3);
    ylim([deltan_vec(1) deltan_vec(end)]*10^3);
    title('\textbf{Constraints 1, 2, \& 3}', 'Interpreter', 'latex')
    xlabel('$\delta_1$ [mm]', 'Interpreter', 'latex');
    ylabel('$\delta_n$ [mm]', 'Interpreter', 'latex');
    legend('$\mathrm{log}_2(N_{min})$', '$\delta_{n_{max}}$ (1)', '$\delta_{n_{max}}$ (3)', '$\delta_{n_{min}}$ (3)', '$\delta_1 = \delta_n$', 'Chosen point', 'Interpreter', 'latex');
end
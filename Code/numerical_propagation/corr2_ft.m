function c = corr2_ft(u1, u2, mask, delta)

% ------------------------------------------
% Function for calculation of 2D discrete correlation without aperture
% effect
%
% Retrieved on December 2020 by
% J. D. Schmidt, "Numerical Simulation of Optical Wave Propagation 
% with examples in MATLAB", SPIE Press Monograph Vol. PM199, 2010
% (Listing 3.5)
% ------------------------------------------

N = size(u1, 1);
c = zeros(N);
delta_f = 1/(N*delta); % frequency grid spacing [m]

U1 = ft2(u1 .* mask, delta); % DFTs of signals
U2 = ft2(u2 .* mask, delta);
U12corr = ift2(conj(U1) .* U2, delta_f);

maskcorr = ift2(abs(ft2(mask, delta)).^2, delta_f) * delta^2;
idx = logical(maskcorr);
c(idx) = U12corr(idx) ./ maskcorr(idx) .* mask(idx);
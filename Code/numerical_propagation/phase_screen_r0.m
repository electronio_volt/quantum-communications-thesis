function r0scrn = phase_screen_r0(z, rF, rytov, nscrn, lambda)

% ------------------------------------------
% Function for calculating the coherence diameters of the layered 
% atmospheric model based on constrained optimization
%
% Adapted from 
% J. D. Schmidt, "Numerical Simulation of Optical Wave Propagation 
% with examples in MATLAB", SPIE Press Monograph Vol. PM199, 2010
% (Listing 9.5)
% ------------------------------------------

k = 2*pi/lambda;

A = zeros(2, nscrn); % matrix
alpha = (0:nscrn-1) / (nscrn-1);
% A(1,:) = alpha.^(5/3);
A(1,:) = ones(size(alpha));
% A(2,:) = (1 - alpha).^(5/6) .* alpha.^(5/6);
A(2,:) = (1 - alpha).^(5/6);
b = [rF.^(-5/3); rytov/1.33*(k/z)^(5/6)];
% initial guess
x0 = (nscrn/3*rF * ones(nscrn, 1)).^(-5/3);
% objective function
fun = @(X) abs(sum((A*X(:) - b).^2));
% constraints
x1 = zeros(nscrn, 1);
rmax = 0.1; % maximum Rytov number per partial prop
x2 = rmax/1.33*(k/z)^(5/6) ./ A(2,:);
x2(A(2,:)==0) = 50^(-5/3);
options = optimoptions(@fmincon, 'Display', 'off') ;
[X,~,~,~] = fmincon(fun,x0,[],[],[],[],x1,x2,[],options);
% check screen r0s
r0scrn = X.^(-3/5);
r0scrn(isinf(r0scrn)) = 1e6;
function g = ift2(G, delta_f)

% ------------------------------------------
% Function for calculating 2D DIFT 
%
% Retrieved on December 2020 by
% J. D. Schmidt, "Numerical Simulation of Optical Wave Propagation 
% with examples in MATLAB", SPIE Press Monograph Vol. PM199, 2010
% (Listing 2.2)
% ------------------------------------------

N = size(G, 1);
g = ifftshift(ifft2(ifftshift(G))) * (N * delta_f)^2;
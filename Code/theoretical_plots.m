clear;
close all;
set(0, 'DefaultLineLineWidth', 1.5);

%% Super-gaussian filter
N = 100;
n = 16;
x = -N/2 : 1 : N/2;
sigma = 0.45*N;

sg = exp(-(x/sigma).^n);

figure;
plot(x + N/2, sg);
ylim([0 1.2]);
title('Super-gaussian filter')
xlabel('Grid index')
ylabel('Attenuation factor')
ax = gca;
ax.GridLineStyle = '--';
grid on

%% Channel parameters
h0 = 0;
H = 500*10^3;
Cn0 = 9.6*10^(-14);

%% NASA US Standard atmosphere calculations

h_division = ceil((H-h0)/1000); % Step size for altitude
h = h0:h_division:H; 
[temp, press, lapse_rate] = std_atmo(h0, H, h_division);

%% AFGL+WK model

S = zeros(1, length(h)); % Wind shear
Cn2 = afgl_wk(h0, H, h_division, temp, press, lapse_rate, Cn0, S);

%% Plots

% Calculate Cn2 from Hufnagel-Valley model (for comparison)
urms = 6;
Cn2_hv = HV_model(h0, H, h_division, urms, Cn0);

% Plot Standard Atmosphere pressure, temperature and lapse rate
figure;
plot(h*10^(-3), press);
title('\textbf{U.S. Standard Atmosphere model}', 'Interpreter', 'latex')
xlabel('Altitude [km]', 'Interpreter', 'latex')
ylabel('Pressure [mbar]', 'Interpreter', 'latex')
grid on;
figure;
plot(h*10^(-3), temp);
title('\textbf{U.S. Standard Atmosphere model}', 'Interpreter', 'latex')
xlabel('Altitude [km]', 'Interpreter', 'latex')
ylabel('Temperature [K]', 'Interpreter', 'latex')
grid on;
figure;
plot(h*10^(-3), lapse_rate)
title('\textbf{U.S. Standard Atmosphere model}', 'Interpreter', 'latex')
xlabel('Altitude [km]', 'Interpreter', 'latex')
ylabel('Lapse Rate [K/km]', 'Interpreter', 'latex')
grid on;

% Plot the structure parameter
figure; 
semilogx(Cn2, h*10^(-3));
hold on;
semilogx(Cn2_hv, h*10^(-3));
xlabel('$C_n^2$ [$\mathrm{m}^{-2/3}$]', 'Interpreter', 'latex')
ylabel('Altitude [km]', 'Interpreter', 'latex')
legend('Standard Atmosphere', 'Hufnagel-Valley', 'Interpreter', 'latex')
title('\textbf{Structure Parameter Model}', 'Interpreter', 'latex')
xlim([10^(-22) 10^(-13)])
ylim([0 35])
grid on;

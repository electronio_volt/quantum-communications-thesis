close all;

%% Optical parameters
lambda = 1550*10^(-9); % Optical wavelength [m]
k = 2*pi / lambda; % Optical wavenumber [rad/m]
Dz = 1.2228e+06; % Propagation distance [m]
w0 = 15*10^(-2); % Beamwaist radius
zR = pi*(w0^2)/lambda; % Rayleigh range
Rp = Dz*(1+(zR/Dz)^2); 
p = 0; % Radial mode number
Lp = 1; % Generalized Laguerre polynomial of order zero
la = [0 1 2 3 4]; % OAM quantum number
l = la(2);

%% Transmitter/receiver apertures
r0lrms_tra = sqrt((max(la)+1)/2)*w0;
r0l_tra = sqrt(2)*r0lrms_tra;
wDz = w0*sqrt(1+(Dz/zR)^2);
r0lrms_rec = sqrt((max(la)+1)/2)*wDz;
r0l_rec = sqrt(2)*r0lrms_rec;
D1 = r0l_tra; % Diameter of the source aperture [m]
D2 = r0l_rec; % Diameter of the observation aperture [m]

%% Sampling constraints
delta1_max = lambda*Dz/D2;
n = 1;
while round(delta1_max, n) == 0
    n = n + 1;
end
if round(delta1_max, n) > delta1_max
    delta1 = 2*delta1_max - round(delta1_max, n);
else
    delta1 = round(delta1_max, n);
end
delta1 = delta1/2;
deltan_max = lambda*Dz/D1 - D2*delta1/D1;
deltan = deltan_max/6; % observation-plane grid spacing [m]
Nmin = 2^(ceil(log2(D1/2/delta1 + D2/2/deltan + lambda*Dz/2/delta1/deltan)));
N = Nmin; % number of grid points
Dz_max = (N/lambda)*(min([delta1 deltan]))^2;
nmin = ceil(Dz/Dz_max) + 1;
n = nmin; % number of partial propagations

%% Free-space propagation
% source-plane coordinates
[x1, y1] = meshgrid((-N/2 : N/2-1) * delta1);
[theta1, r1] = cart2pol(x1, y1);
% switch from total distance to individual distances
z = (1:n) * Dz / n;

z0 = 0;
w = w0*sqrt(1+(z0/zR)^2);
% Lp = -(2*(r1.^2)/w^2) + 1;
phi_in = exp(1j*l*theta1).*2*sqrt(factorial(p)/factorial(p+abs(l)))*(1/w).*((r1*sqrt(2)/w).^abs(l)).*exp(-r1.^2/w^2)*Lp.*exp(1j*k*r1.^2*z0/(2*(z0^2+zR^2)))*exp(-1j*(2*p+abs(l)+1)*atan(z0/zR))/sqrt(2*pi);
[x2, y2, Uout] = ...
ang_spec_multi_prop_vac(phi_in, lambda, delta1, deltan, z);

[xn, yn] = meshgrid((-N/2 : N/2-1) * deltan);
[thetan, rn] = cart2pol(xn, yn);

w = w0*sqrt(1+(Dz/zR)^2);
Uout_theor = exp(1j*l*thetan).*2*sqrt(factorial(p)/factorial(p+abs(l)))*(1/w).*((rn*sqrt(2)/w).^abs(l)).*exp(-rn.^2/w^2)*Lp.*exp(1j*k*rn.^2*Dz/(2*(Dz^2+zR^2)))*exp(-1j*(2*p+abs(l)+1)*atan(Dz/zR))/sqrt(2*pi);

% Plot results
figure;
plot(xn(N/2+1,:), abs(Uout(N/2+1,:)));
hold on; 
plot(xn(N/2+1,:), abs(Uout_theor(N/2+1,:)), 'r-o');
xlim([-D2/2 D2/2])

figure;
[C,h]=contourf(xn,yn,abs(Uout),100);
colorbar
colormap('hot')
h.LineStyle='none';
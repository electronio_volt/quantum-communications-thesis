function [MCF2] = gaussian_MCF2(z, r, p, l, w0, sR2, lambda)
% GAUSSIAN_MCF2
% Function that calculates the Mutual Coherence Function (MCF) of a
% Laguerre-Gaussian beam with parameters:
% - z, the distance from the origin
% - r, the radial coordinate
% - p, the radial mode number
% - l, the angular mode number
% - w0, the beam-waist radius at the origin
% - sR2, the Rytov variance and
% - lambda, the optical wavelength
%
% NOTE: The formula used here is valid for the weak turbulence regime

zR = pi*(w0^2)/lambda; % Rayleigh range
k = 2*pi/lambda; % Optical wavenumber
wL = w0*sqrt(1+(z/zR)^2); % Beam-waist radius at distance z
w = sqrt(2*p + abs(l) + 1)*wL; % Corrected beam-waist radius at distance z

F = z*(1+(zR/z)^2);
Theta = 1 + z/F;
Lambda = 2*z/k/w^2;
T = 1.33*sR2*Lambda^(5/6);
q = 1.22*sR2^(6/5);

if (Theta >= 0)
    alpha = (1 - Theta^(8/3))/(1 - Theta);
else
    alpha = (1 + abs(Theta)^(8/3))/(1 - Theta);
end
MCF2 = ((w0/w)^2)*exp(-T)*exp(-(Lambda*k*r.^2)/4/z - 3*alpha* ...
    (((q*k*r.^2)/z)^(5/6))/8);
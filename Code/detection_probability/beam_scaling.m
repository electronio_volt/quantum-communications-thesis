function [N_same, N_dif] = ...
    beam_scaling(x, r0l_0, r0l_L)
    
    N = length(x);
    
    x_r0l0 = abs(x(N/2+1, :) - r0l_0);
    [~, index_r0l0] = min(x_r0l0);
    x_r0lL = abs(x(N/2+1, :) - r0l_L);
    [~, index_r0lL] = min(x_r0lL);
    N_same = x(N/2+1, index_r0lL)*N/x(N/2+1, index_r0l0);
    if (mod(ceil(N_same), 2) == 0)
        N_same = ceil(N_same);
    else 
        N_same = floor(N_same);
    end
    N_dif = (N_same - N)/2;
function Pl = detection_prob(u_in, u_out, delta_in, delta_out)
    f = u_out.*conj(u_in);
    MCF2_f = conv_fft2(f, rot90(conj(f),2), 'same');
    Pl = sum(MCF2_f, 'all')*(delta_in*delta_out)^2;
end